package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class StatusTelemetryData {
    public static void main(String[] args) {
        //Read data from input file
        File file = new File("telemetryData.txt");
        List<Telemetry> data = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split("\\|");
                Telemetry telemetry = new Telemetry(parts[0],
                        Integer.parseInt(parts[1]),
                        Integer.parseInt(parts[2]),
                        Integer.parseInt(parts[3]),
                        Integer.parseInt(parts[4]),
                        Integer.parseInt(parts[5]),
                        Double.parseDouble(parts[6]),
                        parts[7]);
                //Add to data List
                data.add(telemetry);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //Group data by satelliteId and component type
        Map<String, List<Telemetry>> dataMap = new HashMap<>();
        for (Telemetry t : data) {
            String key = t.getSatelliteId() + "_" + t.getComponent();
            if (!dataMap.containsKey(key)) {
                List<Telemetry> telemList = new ArrayList<>();
                dataMap.put(key, telemList);
            }
            dataMap.get(key).add(t);
        }

        //Create alert messages
        List<Alert> alerts = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd H:mm:ss.SSS");
        for (String key : dataMap.keySet()) {
            int satelliteId = Integer.parseInt(key.split("_")[0]);
            int initialRecordFlag = 0;
            String component = key.split("_")[1];
            List<Telemetry> telemList = dataMap.get(key);

            //Check conditions
            if (component.equals("TSTAT")) {
                //Check if there are 3 readings that exceed red high limit within 5 minutes
                int count = 1;
                for (int i = 0; i < telemList.size() - 1; i++) {
                    try {
                        Date currentTime = dateFormat.parse(telemList.get(i).getTimeStamp());
                        Date nextTime = dateFormat.parse(telemList.get(i+1).getTimeStamp());
                        long diff = nextTime.getTime() - currentTime.getTime();
                        long diffMinutes = diff / (60 * 1000);
                        if (telemList.get(i).getRawValue() > telemList.get(i).getRedHighLimit() && diffMinutes <= 5) {
                            count++;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (count >= 3) {
                    Alert alert = new Alert(satelliteId, "RED HIGH", component, telemList.get(initialRecordFlag).getTimeStamp());
                    alerts.add(alert);
                }
            } else if (component.equals("BATT")) {
                //Check if there are 3 readings that are under red low limit within 5 minutes
                int count = 1;
                for (int i = 0; i < telemList.size() - 1; i++) {
                    try {
                        Date currentTime = dateFormat.parse(telemList.get(i).getTimeStamp());
                        Date nextTime = dateFormat.parse(telemList.get(i+1).getTimeStamp());
                        long diff = nextTime.getTime() - currentTime.getTime();
                        long diffMinutes = diff / (60 * 1000);
                        if (telemList.get(i).getRawValue() < telemList.get(i).getRedLowLimit() && diffMinutes <= 5) {
                            count++;
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (count >= 3) {
                    Alert alert = new Alert(satelliteId, "RED LOW", component, telemList.get(initialRecordFlag).getTimeStamp());
                    alerts.add(alert);
                }
            }
        }
        //Print alert messages
        System.out.println("[");
        int i = 0;
        for (Alert a : alerts) {
            if (!a.getSeverity().equals("")) {
                System.out.println("\t{");
                System.out.println("\t\t\"satelliteId\": " + a.getSatelliteId() + ",");
                System.out.println("\t\t\"severity\": \"" + a.getSeverity() + "\",");
                System.out.println("\t\t\"component\": \"" + a.getComponent() + "\",");
                System.out.println("\t\t\"timestamp\": \"" + a.getTimeStamp() + "\"");
                System.out.println("\t}");
            }
            if(i+1 <= alerts.size()-1)
            {
                System.out.println(",");
            }
            i++;

        }
        System.out.println("]");
    }
}

/**
 *
 */
class Telemetry {
    private String timeStamp;
    private int satelliteId;
    private int redHighLimit;
    private int yellowHighLimit;
    private int yellowLowLimit;
    private int redLowLimit;
    private double rawValue;
    private String component;

    public Telemetry(String timeStamp, int satelliteId, int redHighLimit, int yellowHighLimit, int yellowLowLimit, int redLowLimit, double rawValue, String component) {
        this.timeStamp = timeStamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public int getRedHighLimit() {
        return redHighLimit;
    }

    public int getYellowHighLimit() {
        return yellowHighLimit;
    }

    public int getYellowLowLimit() {
        return yellowLowLimit;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public String getComponent() {
        return component;
    }
}

class Alert {
    private int satelliteId;
    private String severity;
    private String component;
    private String timeStamp;

    public Alert(int satelliteId, String severity, String component, String timeStamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timeStamp = timeStamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public String getComponent() {
        return component;
    }

    public String getTimeStamp() {
        return timeStamp;
    }
}
package org.example;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AlertTest {
    @Test
    public void testGetSatelliteId() {
        Alert alert = new Alert(1, "high", "solar panel", "2020-01-01");
        int expected = 1;
        int actual = alert.getSatelliteId();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetSeverity() {
        Alert alert = new Alert(1, "high", "solar panel", "2020-01-01");
        String expected = "high";
        String actual = alert.getSeverity();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetComponent() {
        Alert alert = new Alert(1, "high", "solar panel", "2020-01-01");
        String expected = "solar panel";
        String actual = alert.getComponent();
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTimeStamp() {
        Alert alert = new Alert(1, "high", "solar panel", "2020-01-01");
        String expected = "2020-01-01";
        String actual = alert.getTimeStamp();
        assertEquals(expected, actual);
    }
}
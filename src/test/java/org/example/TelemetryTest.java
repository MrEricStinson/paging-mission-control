package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TelemetryTest {

    @Test
    void testConstructor() {
        Telemetry telemetry = new Telemetry("12/3/2020", 1, 50, 40, 30, 20, 10.0, "Temperature");
        assertEquals("12/3/2020", telemetry.getTimeStamp());
        assertEquals(1, telemetry.getSatelliteId());
        assertEquals(50, telemetry.getRedHighLimit());
        assertEquals(40, telemetry.getYellowHighLimit());
        assertEquals(30, telemetry.getYellowLowLimit());
        assertEquals(20, telemetry.getRedLowLimit());
        assertEquals(10.0, telemetry.getRawValue());
        assertEquals("Temperature", telemetry.getComponent());
    }
}